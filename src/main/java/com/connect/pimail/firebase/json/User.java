package com.connect.pimail.firebase.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by kieran.mann on 20/11/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    private String fullname;
    private String email;
    private String postcode;
    private boolean signedIn;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSignedIn() {
        return signedIn;
    }

    public void setSignedIn(boolean signedIn) {
        this.signedIn = signedIn;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
}
