package com.connect.pimail.firebase;

import com.connect.pimail.firebase.json.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kieran.mann on 20/11/2015.
 */
public class FirebaseService {


    private List<User> getUsers() {

        List<User> returnUsers = new ArrayList<User>();

        String buildUrl = "https://sizzling-fire-2100.firebaseio.com/users.json";

        HttpURLConnection conn = null;

        try {

            URL url = new URL(buildUrl);

            conn = (HttpURLConnection) url.openConnection();

            if (conn.getResponseCode() != 200) {
                throw new IOException(conn.getResponseMessage());
            }

            // Buffer the result into a string
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }

            ObjectMapper mapper = new ObjectMapper();

            JSONObject users = new JSONObject(sb.toString());

            for (Iterator it = users.keys(); it.hasNext(); ) {

                JSONObject user = users.getJSONObject((String) it.next());

                returnUsers.add(mapper.readValue(user.toString(), User.class));

            }

        } catch (MalformedURLException e) {

            System.out.println("URL is malformed");

        } catch (IOException e) {

            System.out.println(e);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return returnUsers;

    }

    public List<User> getLoggedIn() {

        List<User> usersLoggedIn = new ArrayList<User>();

        for (User user : getUsers()) {

            if (user.isSignedIn()) {
                usersLoggedIn.add(user);

            }
        }

        return usersLoggedIn;
    }

}
