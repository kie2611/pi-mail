package com.connect.pimail.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by kieran.mann on 19/11/2015.
 */
public class PropertyReader {

    public Properties getProp(String propertyName) {

        Properties props = new Properties();

        InputStream in = getClass().getClassLoader().getResourceAsStream(propertyName);

        if (in != null) {

            try {
                props.load(in);

            } catch (IOException e){

                System.out.println("Failed to load properties");
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }
            }


        } else {
            System.out.println("property file '" + propertyName + "' not found in the classpath");
        }

        return props;
    }

}
