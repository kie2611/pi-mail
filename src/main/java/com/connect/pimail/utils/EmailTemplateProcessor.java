package com.connect.pimail.utils;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.resourceresolver.ClassLoaderResourceResolver;
import org.thymeleaf.templateresolver.TemplateResolver;


/**
 * Created by kieran.mann on 20/11/2015.
 */
public class EmailTemplateProcessor extends TemplateEngine {


    public EmailTemplateProcessor() {

        TemplateResolver templateResolver = new TemplateResolver();

        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setResourceResolver(new ClassLoaderResourceResolver());

        this.setTemplateResolver(templateResolver);

    }


}
