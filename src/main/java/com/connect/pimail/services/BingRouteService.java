package com.connect.pimail.services;

import com.connect.pimail.utils.PropertyReader;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kieran.mann on 19/11/2015.
 */
public class BingRouteService {

    private static final String PROP_FILE = "general.properties";
    private final static PropertyReader propReader = new PropertyReader();

    ObjectMapper mapper = new ObjectMapper();

    public List<RouteBean> getTravelTime(String from, String to) {

        List<RouteBean> returnRoutes = new ArrayList<RouteBean>();

        String buildUrl = "http://dev.virtualearth.net/REST/v1/Routes/Driving?";

        //From and To locations
        buildUrl += "wp.1=" + from.replaceAll(" ", "%20");
        buildUrl += "&wp.2=" + to.replaceAll(" ", "%20");

        //Return 3 possible routes
        buildUrl += "&maxSolns=3";
        buildUrl += "&distanceUnit=mi";

        String apiKey = propReader.getProp(PROP_FILE).getProperty("bing.apikey");
        buildUrl += "&key=" + apiKey;

        try {

            URL url = new URL(buildUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            if (conn.getResponseCode() != 200) {
                throw new IOException(conn.getResponseMessage());
            }

            // Buffer the result into a string
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }

            JsonParser parser = new JsonParser();

            JsonObject root = parser.parse(sb.toString()).getAsJsonObject();

            JsonArray resourceSets = root.getAsJsonArray("resourceSets");


            for (int i = 0; i < resourceSets.size(); i++) {

                JsonObject resSetItem = (JsonObject) resourceSets.get(i);
                JsonArray routes = (JsonArray) resSetItem.get("resources");

                //Now we have obtained the subset of json containing the route

                List<Route> routeList = mapper.readValue(routes.toString(), new TypeReference<List<Route>>() {
                });

                if (routeList != null) {

                    for (Route route : routeList) {

                        returnRoutes.add(createBean(route));

                    }
                }
            }


        } catch (MalformedURLException e) {

            System.out.println("URL is malformed");

        } catch (IOException e) {

            System.out.println(e);
        }


        return returnRoutes;
    }


    private RouteBean createBean(Route route) {

        RouteBean routeBean = new RouteBean();

        routeBean.setDescription(route.getRouteLegs().get(0).getDescription());
        routeBean.setTravelDuration(route.getTravelDuration());
        routeBean.setTravelDurationTraffic(route.getTravelDurationTraffic());
        routeBean.setTravelDistance(route.getTravelDistance());

        return routeBean;

    }

}
