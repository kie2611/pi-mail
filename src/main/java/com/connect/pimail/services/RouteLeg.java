package com.connect.pimail.services;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by kieran.mann on 23/11/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteLeg {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
