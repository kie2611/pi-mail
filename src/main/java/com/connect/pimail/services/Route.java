package com.connect.pimail.services;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by kieran.mann on 20/11/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Route {


    private Integer travelDistance;

    private Integer travelDuration;

    private Integer travelDurationTraffic;

    private List<RouteLeg> routeLegs;

    public Integer getTravelDistance() {
        return travelDistance;
    }

    public void setTravelDistance(Integer travelDistance) {
        this.travelDistance = travelDistance;
    }


    public Integer getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(Integer travelDuration) {
        this.travelDuration = travelDuration/60;
    }

    public Integer getTravelDurationTraffic() {
        return travelDurationTraffic;
    }

    public void setTravelDurationTraffic(Integer travelDurationTraffic) {
        this.travelDurationTraffic = travelDurationTraffic/60;
    }

    public List<RouteLeg> getRouteLegs() {
        return routeLegs;
    }

    public void setRouteLegs(List<RouteLeg> routeLegs) {
        this.routeLegs = routeLegs;
    }
}
