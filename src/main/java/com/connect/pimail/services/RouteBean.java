package com.connect.pimail.services;

/**
 * Created by kieran.mann on 23/11/2015.
 */
public class RouteBean {

    private Integer travelDistance;

    private Integer travelDuration;

    private Integer travelDurationTraffic;

    private String description;

    public Integer getTravelDistance() {
        return travelDistance;
    }

    public void setTravelDistance(Integer travelDistance) {
        this.travelDistance = travelDistance;
    }

    public Integer getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(Integer travelDuration) {
        this.travelDuration = travelDuration;
    }

    public Integer getTravelDurationTraffic() {
        return travelDurationTraffic;
    }

    public void setTravelDurationTraffic(Integer travelDurationTraffic) {
        this.travelDurationTraffic = travelDurationTraffic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
