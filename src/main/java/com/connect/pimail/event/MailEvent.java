package com.connect.pimail.event;

/**
 * Created by kieran.mann on 19/11/2015.
 */
public abstract class MailEvent {

    private int sentCount;

    public int getSentCount() {
        return sentCount;
    }

    public void incrementSentCount(){
        sentCount++;
    }

    public abstract void process();



}
