package com.connect.pimail.event;

import com.connect.pimail.emailer.EmailDetails;
import com.connect.pimail.emailer.EmailProcessor;
import com.connect.pimail.firebase.FirebaseService;
import com.connect.pimail.firebase.json.User;
import com.connect.pimail.services.BingRouteService;
import com.connect.pimail.services.RouteBean;
import com.connect.pimail.utils.EmailTemplateProcessor;
import org.thymeleaf.context.Context;

import java.util.List;
import java.util.Locale;

/**
 * Created by kieran.mann on 19/11/2015.
 */
public class EndDayMailEvent extends MailEvent {

    public void process(){

        FirebaseService firebase = new FirebaseService();

        EmailProcessor emailer = new EmailProcessor();

        List<User> users = firebase.getLoggedIn();

        for (User user: users) {

            emailer.sendEmail(buildEmail(user));

        }

    }


    private EmailDetails buildEmail(User user) {

        EmailTemplateProcessor emailProcessor = new EmailTemplateProcessor();

        final Context ctx = new Context(Locale.ENGLISH);

        ctx.setVariable("name", user.getFullname());

        if (user.getPostcode() != null && user.getPostcode() != "") {
            ctx.setVariable("to", user.getPostcode());
            ctx.setVariable("routes", getTravelInfo(user.getPostcode()));
        }
        final String htmlContent = emailProcessor.process("email.html",ctx);

        EmailDetails email = new EmailDetails();

        email.setToAddress(user.getEmail());
        email.setSubject("Goodnight " + user.getFullname() + "! Here's your journey info...");
        email.setBody(htmlContent);


        return email;
    }



    private List<RouteBean> getTravelInfo(String postcode){

        BingRouteService gog = new BingRouteService();

        List<RouteBean> routes = gog.getTravelTime("B377ES", postcode);



        return routes;
    }

}
