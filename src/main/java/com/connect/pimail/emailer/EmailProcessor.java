package com.connect.pimail.emailer;

import com.connect.pimail.utils.PropertyReader;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.ldap.UnsolicitedNotification;

/**
 * Created by kieran.mann on 19/11/2015.
 */
public class EmailProcessor {

    private final static String EMAIL_PROPS = "email.properties";

    private final static PropertyReader propReader = new PropertyReader();
    private static Properties emailProps = null;

    private static String username;
    private static String password;

    private Session session;

    public EmailProcessor() {

        if (username == null || password == null) {

            emailProps = propReader.getProp(EMAIL_PROPS);
            username = emailProps.get("pi.email.username").toString();
            password = emailProps.get("pi.email.password").toString();
        }

        if (session == null) {
            session = Session.getInstance(emailProps,
                    new javax.mail.Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });
        }

    }


    public void sendEmail(EmailDetails emailDetails) {

        try {

            MimeMessage message = new MimeMessage(session);

            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(emailDetails.getToAddress()));
            message.setSubject(emailDetails.getSubject());
            message.setText(emailDetails.getBody());

            message.setContent(emailDetails.getBody(), "text/html; charset=utf-8");


            try {
                message.setFrom(new InternetAddress("pi@connect-group.com", "Connect Concierge"));

            } catch (UnsupportedEncodingException e ) {

                System.out.println(e);

            }
            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }


}
