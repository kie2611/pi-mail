package com.connect.pimail;

import com.connect.pimail.event.EndDayMailEvent;

/**
 * Created by kieran.mann on 19/11/2015.
 */
public class PiMailRunner {

    public static void main(String[] args) {

        EndDayMailEvent endDayMailEvent = new EndDayMailEvent();
        endDayMailEvent.process();

    }
}
